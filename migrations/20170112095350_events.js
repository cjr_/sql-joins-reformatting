
exports.up = function(knex, Promise) {
  return knex.schema.createTable('event', (table) => {
    table.increments();
    table.text('name');
    table.text('location');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('event');
};
