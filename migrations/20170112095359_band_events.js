
exports.up = function(knex, Promise) {
  return knex.schema.createTable('band_event', (table) => {
    table.increments();
    table.integer('band_id').unsigned().references('band.id').onDelete('cascade');
    table.integer('event_id').unsigned().references('event.id').onDelete('cascade');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('band_event');
};
