
exports.up = function(knex, Promise) {
  return knex.schema.createTable('band', (table) => {
    table.increments();
    table.text('name');
    table.text('hometown');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('band');
};
