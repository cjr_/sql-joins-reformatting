
exports.seed = function(knex, Promise) {
  return knex.raw('DELETE from band; ALTER SEQUENCE band_id_seq restart with 4;')
    .then(function () {
      const bands = [{
        id: 1,
        name: 'Green Day',
        hometown: 'East Bay'
      }, {
        id: 2,
        name: 'Vampire Weekend',
        hometown: 'NYC'
      }, {
        id: 3,
        name: 'Tycho',
        hometown: 'Sacramento'
      }];

      return knex('band').insert(bands);
    });
};
