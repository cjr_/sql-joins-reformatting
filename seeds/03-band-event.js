
exports.seed = function(knex, Promise) {
  return knex('band_event').del()
    .then(function () {
      const band_events = [{
        band_id: 1,
        event_id: 1
      }, {
        band_id: 1,
        event_id: 2
      }, {
        band_id: 1,
        event_id: 3
      }, {
        band_id: 2,
        event_id: 1
      }, {
        band_id: 2,
        event_id: 2
      }, {
        band_id: 2,
        event_id: 3
      }, {
        band_id: 3,
        event_id: 1
      }, {
        band_id: 3,
        event_id: 2
      }, {
        band_id: 3,
        event_id: 3
      }]

      return knex('band_event').insert(band_events);
    });
};
