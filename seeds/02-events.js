
exports.seed = function(knex, Promise) {
  return knex.raw('DELETE from event; ALTER SEQUENCE event_id_seq restart with 4;')
    .then(function () {
      const events = [{
        id: 1,
        name: 'Lollapalooza',
        location: 'Chicago'
      }, {
        id: 2,
        name: 'Warped Tour',
        location: 'Denver'
      }, {
        id: 3,
        name: 'Burning Man',
        location: 'Black Rock City'
      }];

      return knex('event').insert(events);
    });
};
