const ezc = require('express-zero-config');

const router = ezc.createRouter();

const knex = require('knex')({
	client: 'pg',
	connection: 'postgres://localhost/joins-are-fun'
});

router.get('/', (req, res, next) => {
  res.json({
    message: 'Hello World!'
  });
});

function reformatBands(bands) {
	const reformatted = [];

	const bandsById = {};

	bands.forEach(band => {
		if(bandsById[band.band_id]) {
			bandsById[band.band_id].events.push({
				name: band.event_name,
				location: band.location,
				event_id: band.event_id
			});
		} else {
			bandsById[band.band_id] = {
				band_name: band.band_name,
				band_id: band.band_id,
				hometown: band.hometown,
				events: [{
					name: band.event_name,
					location: band.location,
					event_id: band.event_id
				}]
			};

			reformatted.push(bandsById[band.band_id]);
		}
	});

	return reformatted;
}

router.get('/band', (req, res, next) => {
	knex('band')
		.select(
			'band.name as band_name',
			'band.id as band_id',
			'band.hometown',
			'event.name as event_name',
			'event.location',
			'event.id as event_id')
		.join('band_event', 'band_event.band_id', 'band.id')
		.join('event', 'event.id', 'band_event.event_id')
		.then(bands => {
			const reformatted = reformatBands(bands);
			res.json(reformatted)
		});
});

router.get('/event', (req, res, next) => {
	knex('event').then(events => res.json(events));
});

ezc.startServer(router);
